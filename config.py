import sys
import json
from os import path

config_file_name = sys.argv[1] if len(sys.argv) > 1 else "config.json"
if path.exists(config_file_name):
    print("Config is: %s" % config_file_name)
else:
    print("Config doesn't exist:\n%s" % config_file_name)
    exit()
with open(config_file_name, 'r') as config_file:
    configs = json.load(config_file)

PRINT_DEBUG = configs["general"]["print_debug"]
PRINT_UTIL_INFO = configs["general"]["print_util_info"]
OVERLOAD_THRESHOLD = configs["general"]["overload_threshold"]
FINAL_SCALE = configs["general"]["final_scale"]
FINAL_MAX_WIDTH = configs["general"]["final_max_width"]
FINAL_MAX_HEIGHT = configs["general"]["final_max_height"]

UTIL_FUNCTION_MODE = configs["util_function"]["mode"]

INPUT_FILE_FORMAT = configs["input"]["input_format"]
SORT_LIST = configs["input"]["sort_list"]
LIST_BASE = configs["input"]["list_mode"]
LIST_FILE_NAME = configs["input"]["list_mode_configs"]["list_file_name"]
BY_NAME = configs["input"]["list_mode_configs"]["by_name"]

FINAL_FILE_NAME = configs["output"]["name"]
QUALITY = configs["output"]["quality"]
SAVE_ROW_WISE = configs["output"]["save_each_row"]["enable"]
ROW_WISE_FORMAT = configs["output"]["save_each_row"]["name_format"]
SAVE_WALLPAPER_ENGINE = configs["output"]["save_each_row"]["save_wallpaper_engine"]
WALLPAPER_ENGINE_FILE_NAME = configs["output"]["save_each_row"]["wallpaper_engine_file_name"]
