import re
import json

uid = 1

def get_uid():
    global uid
    uid += 1
    return uid - 1

def get_wallpaper_engine_object(full_name, speed, width, height, y_origin):
    file_name = re.findall(r"/([^/]*)[.]jpg", full_name)[0]
    image_name = "models/{}.json".format(file_name);
    uid1 = get_uid()
    uid2 = get_uid()
    uid3 = get_uid()
    speed = 0.1*speed
    scale_val = 1
    origin = "{} {} 0".format(int(width / 2), y_origin)
    scale = ("{} ".format(scale_val) * 2) + "1"
    size = "{} {}".format(width, height)
    return {
        "angles" : "0.000 0.000 0.000",
        "colorBlendMode" : 0,
        "copybackground" : True,
        "effects" :
        [
            {
                "file" : "effects/scroll/effect.json",
                "id" : uid3,
                "name" : "",
                "passes" :
                [
                    {
                        "constantshadervalues" :
                        {
                            "speedx" : speed,
                            "speedy" : 0
                        },
                        "id" : uid2
                    }
                ],
                "visible" : True
            }
        ],
        "id" : uid1,
        "image" : image_name,
        "ledsource" : False,
        "locktransforms" : False,
        "name" : file_name,
        "origin" : origin,
        "parallaxDepth" : "1.000 1.000",
        "perspective" : False,
        "scale" : scale,
        "size" : size,
        "solid" : True,
        "visible" : True
    }

def create_wallpaper_engine_config(row_infos):
    speed = 1
    y_origin = 0
    we_objects = []
    for full_name, image in row_infos.items():
        size = image.size
        y_origin += int(size[1] / 2)
        we_object = get_wallpaper_engine_object(full_name, speed, size[0], size[1], 1080 - y_origin)
        we_objects.append(we_object)
        y_origin += int(size[1] / 2)
        speed = -speed
    return json.dumps(we_objects)
