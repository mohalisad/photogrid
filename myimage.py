from PIL import Image

class MyImage:
    def __init__(self, img_path):
        self.img_path = img_path
        with Image.open(self.img_path) as img:
            self.orig_width = img.size[0]
            self.orig_height = img.size[1]
        self.width = self.orig_width
        self.height = self.orig_height
        self.need_to_load = True

    def get_size(self):
        return (int(self.width), int(self.height))

    def get_width(self):
        return int(self.width)

    def get_height(self):
        return int(self.height)

    def resize_to_width(self, new_width):
        self.height = new_width * self.height / self.width
        self.width = new_width
        self.need_to_load = True

    def resize_to_height(self, new_height):
        self.width = new_height * self.width / self.height
        self.height = new_height
        self.need_to_load = True

    def resize_to_scale(self, scale):
        self.width *= scale
        self.height *= scale
        self.need_to_load = True

    def calc_util_scale(self):
        return self.width / self.orig_width

    def get_img_path(self):
        return self.img_path

    def get_resized_img(self):
        if self.need_to_load:
            with Image.open(self.img_path) as img:
                self.img = img.resize(self.get_size(), Image.ANTIALIAS)
            self.need_to_load = False
        return self.img

    def change_width(self, change_val):
        self.width += change_val

    def change_height(self, change_val):
        self.height += change_val

    def destruct(self):
        del self.img
        self.need_to_load = True
