import math
from PIL import Image

def get_row_height(row):
    return row[0].get_height()

def get_row_width(row):
    return sum(img.get_width() for img in row)

def get_rows_height(rows):
    return sum(get_row_height(row) for row in rows)

def paste_row_in_image(dst, row, y_offset = 0):
    x_offset = 0
    for img in row:
        dst.paste(img.get_resized_img(), (x_offset, y_offset))
        x_offset += img.get_width()

class MyImageMerger:
    def __init__(self, my_images, row_count,  overload_threshold = 0, util_mode = "avg"):
        self.images = my_images
        self.row_image_count = row_count
        self.util_mode = util_mode
        self.overload_threshold = overload_threshold

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        for img in self.images:
            img.destruct()

    def merge_and_scale(self, scale, max_width, max_height):
        self.__split_to_rows()
        self.__scale_each_image_same_height()
        self.__scale_final_width(scale, max_width)
        self.__scale_final_height(max_height)
        self.__fill_each_row_end_blank()
        self.__fill_rows_end_blank()

    def get_final_image(self, verbose = False, verbose_function = lambda x: None):
        new_im = Image.new('RGB', (self.final_width, self.final_height))
        y_offset = 0
        image_finished_counter = 0
        for index, row in enumerate(self.rows):
            paste_row_in_image(new_im, row, y_offset)
            y_offset += row[0].get_height()
            image_finished_counter += len(row)
            if verbose:
                verbose_function(image_finished_counter)
        return new_im

    def get_each_row(self, row_name_format):
        row_infos = {}
        for index, row in enumerate(self.rows):
            row_im = Image.new('RGB', (self.final_width, get_row_height(row)))
            paste_row_in_image(row_im, row)
            row_infos[row_name_format.format(index)] = row_im
        return row_infos

    def get_stats(self):
        stats = {"high":{}}
        utils = []
        for img in self.images:
            img_util_scale = img.calc_util_scale()
            if img_util_scale >= 1:
                stats["high"][img.get_img_path()] = img_util_scale
            utils.append(img_util_scale)
        stats["min"] = min(utils)
        stats["max"] = max(utils)
        stats["avg"] = sum(utils) / len(utils)
        return stats

    def __util_function(self, inp):
        if self.util_mode == "avg":
            return_value = int(sum(inp) / len(inp))
        if self.util_mode == "min":
            return_value = min(inp)
        if self.util_mode == "max":
            return_value = max(inp)
        return return_value

    def __calc_row_sizes(self):
        overload = len(self.images) % self.row_image_count
        row_sizes = [self.row_image_count] * math.floor(len(self.images) / self.row_image_count)
        index = 0
        if overload <= self.overload_threshold and len(row_sizes) != 0:
            while overload > 0:
                row_sizes[index] += 1
                overload -= 1
                index = (index +  1) % len(row_sizes)
        if overload != 0:
            row_sizes.append(overload)
        return row_sizes

    def __split_to_rows(self):
        self.rows = []
        row_sizes = self.__calc_row_sizes()
        pointer = 0
        for size in row_sizes:
            next_pointer = pointer + size
            sub_images = self.images[pointer : next_pointer]
            self.rows.append(sub_images)
            pointer = next_pointer

    def __scale_each_image_same_height(self):
        for row in self.rows:
            new_height = self.__util_function([img.get_height() for img in row])
            for img in row:
                img.resize_to_height(new_height)

    def __scale_final_width(self, scale, max_width):
        final_width = self.__util_function([get_row_width(row) for row in self.rows])
        self.final_width = min(int(scale * final_width), max_width)
        for row in self.rows:
            scale_val = self.final_width / get_row_width(row)
            for img in row:
                img.resize_to_scale(scale_val)

    def __scale_all(self, scale):
        for row in self.rows:
            for img in row:
                img.resize_to_scale(scale)

    def __scale_final_height(self, max_height):
        self.final_height = get_rows_height(self.rows)
        if self.final_height > max_height:
            self.final_height = max_height
            self.final_width = int(self.final_width * self.final_height / get_rows_height(self.rows))
            self.__scale_all(self.final_height / get_rows_height(self.rows))

    def __fill_each_row_end_blank(self):
        for row in self.rows:
            index = 0
            diff_width = self.final_width - get_row_width(row)
            sgn = 1 if diff_width > 0 else -1
            while diff_width != 0:
                row[index].change_width(sgn)
                index = (index + 1) % len(row)
                diff_width -= sgn

    def __fill_rows_end_blank(self):
        index = 0
        diff_height = self.final_height - get_rows_height(self.rows)
        sgn = 1 if diff_height > 0 else -1
        while diff_height != 0:
            for img in self.rows[index]:
                img.change_height(sgn)
            index = (index + 1) % len(self.rows)
            diff_height -= sgn
