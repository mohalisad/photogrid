import glob
import re

def compare_function(item, by_name):
    name, date = item
    name_chunks = name.lower().split(" ")
    base_name = name_chunks[0]
    if name_chunks[0] in ["the", "a", "super"]:
        name_chunks = name_chunks[1:]
    if "of" in name_chunks:
        i = name_chunks.index("of")
        if i > 0 and i + 1 < len(name_chunks):
            name_chunks[i - 1] = name_chunks[i - 1] + name_chunks[i + 1]
    base_name = name_chunks[0]
    if by_name:
        return "{}{}".format(base_name, date)
    else:
        return "{}{}".format(date, base_name)

class FileManager:
    def __init__(self, file_format):
        self.file_format = file_format
        self.files = []

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def read_from_csv(self, csv_content):
        names_and_date = csv_content.splitlines()
        for item in names_and_date:
            name, date = item.split(",")
            self.files.append([name, date])

    def descover_all(self):
        for file in glob.glob(self.file_format.format("*")):
            file = file.replace("\\","/")
            file_name = re.findall(r"/([^/]*)[.]", file)[0]
            self.files.append([file_name, 0])

    def sort_by_name(self):
        self.files.sort(key = lambda x: compare_function(x, True))

    def sort_by_date(self):
        self.files.sort(key = lambda x: compare_function(x, False))

    def sort_by_int_name(self):
        self.files.sort(key = lambda x: int(x[0]))

    def return_list(self):
        return [self.file_format.format(file_name) for file_name, date in self.files]
