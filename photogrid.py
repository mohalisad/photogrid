from file_manager import FileManager
from myimage import MyImage
from myimage_merger import MyImageMerger
from config import *
from we import *

if __name__ == "__main__":
    print("Number of columns:")
    columns_len = int(input())

    with FileManager(INPUT_FILE_FORMAT) as file_manager:
        if LIST_BASE:
            with open(LIST_FILE_NAME, 'r') as content_file:
                file_manager.read_from_csv(content_file.read())
        else:
            file_manager.descover_all()

        if SORT_LIST:
            if LIST_BASE:
                if BY_NAME:
                    file_manager.sort_by_name()
                else:
                    file_manager.sort_by_date()
            else:
                file_manager.sort_by_int_name()

        files = file_manager.return_list()

    images = [MyImage(file) for file in files]

    with MyImageMerger(images, columns_len, OVERLOAD_THRESHOLD, UTIL_FUNCTION_MODE) as merger:
        merger.merge_and_scale(FINAL_SCALE, FINAL_MAX_WIDTH, FINAL_MAX_HEIGHT)
        final_img = merger.get_final_image(PRINT_DEBUG, lambda t: print(t))
        final_img.save(FINAL_FILE_NAME, quality = QUALITY, progressive = True, optimize = True)

        if SAVE_ROW_WISE:
            rows = merger.get_each_row(ROW_WISE_FORMAT)
            for name, row_img in rows.items():
                    row_img.save(name, quality = QUALITY, progressive = True, optimize = True)

            if SAVE_WALLPAPER_ENGINE:
                with open(WALLPAPER_ENGINE_FILE_NAME, 'w') as f:
                    f.write(create_wallpaper_engine_config(rows))

        if PRINT_UTIL_INFO:
            stats = merger.get_stats()
            for name, scale in stats["high"].items():
                print("{}: {}".format(name, scale))
            print("Min util: {}".format(stats["min"]))
            print("Max util: {}".format(stats["max"]))
            print("Avg util: {}".format(stats["avg"]))

    print("Press enter to exit...", end ="")
    input()
